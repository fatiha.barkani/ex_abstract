<?php
namespace Combat;

abstract class Personne {

    public $arme;
    public $vitesse;
    public $force;

    public function __construct($arme, $vitesse, $force){
        $this->arme = $arme;
        $this->vitesse = $vitesse;
        $this->force = $force;
    }

    abstract public function courrir();
    public function hello(){
        echo "hello";
    }

}